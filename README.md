# Musik Liberation

**Short version:**  
A place where you can listen and download music freely. If you want to help the authors, you can watch ads before downloading the content. Musik Liberation is a community so you should help it (see [CONTRIBUTING.md](https:/)).

**Long version:**  
Currently, music is a business. The big music enterprises always want to make more and more and more money. That's why you can't  download or listen music without paying or listening annoying ads. Hate those people that just make music to win money. Music is a way of expressing yourself, not a way to make business. That's why I've started with Musik Liberation: to free the music. Everyone should be able to listen and enjoy music without having a big enterprise trading with their data or saying them what to listen and how to do it. Listen to what you want and how you want!

## Getting Started

Musik Liberation isn't already done. You can install it if you want but that doesn't make any sense.

### Prerequisites

Before installing Musik Liberation you'll need:
* [NodeJS](https://nodejs.org/)
* [MongoDB](https://mongodb.com)

### Installing

Once you have NodeJS and MongoDB installed, you have to:

1- Clone the git repository  

`git clone https://gitlab.com/Nefix/MusikLiberation`

2- Install the packages required  

`cd MusikLiberation`  
`npm install --save`

3- Create a directory to the data of the DB  

`mkdir data`  

4- Run the DB  

`mongod --dbpath data/`

3- Open a new terminal and run it!  

`DEBUG=musikliberation:* npm start`

## Built With

* [NodeJS](https://nodejs.org/) - The server
* [ExpressJS](https://expressjs.com/) - The web framework used
* [Express Generator](https://www.npmjs.com/package/express-generator) - A tool to initialize a Express project
* [EJS](https://www.embeddedjs.com/) - Used to generate the html views
* [MongoDB](https://www.mongodb.com/) - The database used
* [Mongoose](https://mongoosejs.com/) - The framework used to communicate the web with the DB

## Contributing

Please read [CONTRIBUTING.md](https:/)

## Authors

* **Néfix Estrada** - *Currently, the only one :(* - [Néfix Estrada](https://nefixestrada.github.io)


## License

This project is licensed under the GNU GPL v3 License - see the [LICENSE.md](LICENSE.md) file for details
